# OWL2DL-Change-Modularisation

Implementation of belief base change operations for Description Logic ontologies in OWL / OWL 2 using locality-based modules for optimisation.

Please also check:

* Our contributing guidelines: CONTRIBUTING.md
* Our code of conduct: CODE\_OF\_CONDUCT.md

## Building

We use Gradle to build the application:

1. Set up Gradle: https://gradle.org/install/ (we strongly recommend setting up gradle daemon too).
2. Clone this repository.
3. Place the owl2dl-stratification-0.0.2.jar the libs/ directory (compile from https://gitlab.com/rfguimaraes/owl2dlstratification).
4. Open the root directory and type: ```./gradlew check```.
5. If everything is Ok, type: ```./gradlew build```. This should build the application and run the unit tests.

## Regarding performance

We use Gradle's JMH plugin only to generate the jar file. Instead of using the
plugin's "jmh" task, the proper execution of the performance depends on the
tasks jmhDistZip or jmhDistTar. These tasks create the distributable archives
that should be extracted and used to run the benchmarks.
