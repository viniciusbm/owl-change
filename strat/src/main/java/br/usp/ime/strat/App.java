/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.strat;


import br.usp.ime.owl2dlccc.CaseInstance;
import br.usp.ime.owl2dlccc.ChangeCaseCreator;
import br.usp.ime.owl2dlstratification.poset.ADHeightStrictPoset;
import br.usp.ime.owl2dlstratification.poset.AxiomSpecificityStrictPoset;
import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owl2dlstratification.strata.StrataFromStrictPoset;
import br.usp.ime.owlchange.CaseSignatureExtractor;
import br.usp.ime.owlchange.IntegrityConstraintsViolationChecker;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSQueue;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.maxnon.full.MaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.mod.StratifiedModMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.BlackBoxMaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.ClassicalMaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.TrivialMaxNonShrinker;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import javax.annotation.Nonnull;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.io.ReaderDocumentSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.FreshEntityPolicy;
import org.semanticweb.owlapi.reasoner.IndividualNodeSetPolicy;
import org.semanticweb.owlapi.reasoner.NullReasonerProgressMonitor;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import uk.ac.manchester.cs.atomicdecomposition.AtomicDecompositionImpl;
import uk.ac.manchester.cs.owl.owlapi.OWLDeclarationAxiomImpl;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;

public class App {

  static class DummyStrata implements Strata<OWLAxiom> {

    Set<OWLAxiom> axioms;

    public DummyStrata(Set<OWLAxiom> axioms) {
      this.axioms = axioms;
    }

    @Override
    public Optional<Integer> getRank(OWLAxiom axiom) {
      return Optional.of(0);
    }

    @Override
    public Optional<Set<OWLAxiom>> getLayer(int i) {
      return Optional.empty();
    }

    @Override
    public int size() {
      return 1;
    }

    @Nonnull
    @Override
    public Iterator<Set<OWLAxiom>> iterator() {
      List<Set<OWLAxiom>> list = new ArrayList<>(1);
      list.add(axioms);
      return list.iterator();
    }

    @Nonnull
    @Override
    public Iterator<Set<OWLAxiom>> reverseIterator() {
      return iterator();
    }
  }

  private static int timeout_ms = 300000;

  public static void main(String[] args) throws IOException, OWLOntologyCreationException {

    String directory = args[0];
    String prefix = args[1];
    Integer processTimeoutMinutes = Integer.valueOf(args[2]);
    timeout_ms = Integer.parseInt(args[3]) * 1000;

    ExecutorService executorService = Executors.newSingleThreadExecutor();
    Future<String> runResult = executorService.submit(() -> run(directory, prefix));
    Thread thread = null;
    try {
      long processTimeoutMilliseconds = processTimeoutMinutes * 60 * 1000;
      thread = new ChangeCaseCreator.TimingThread((long) (processTimeoutMilliseconds * 1.2));
      thread.start();
      String result = runResult.get(processTimeoutMilliseconds, TimeUnit.MILLISECONDS);
      thread.interrupt();
    } catch (InterruptedException | ExecutionException | TimeoutException e) {
      System.err.println(e.getMessage());
    } finally {
      System.out.println("SHUTDOWN");
      if (thread != null && thread.isAlive()) {
        thread.interrupt();
      }
      executorService.shutdownNow();
    }
    System.exit(0);
  }

  private static String run(String directory, String prefix) {
    String differential = "A";
    String caseIdentifier = "0singleSubClassOfContract";
    String tail = "_1_onto.owl.xml";
    String catalystSuffix = "_contraction";

    String ontoName = String.join(".", prefix, differential, caseIdentifier) + tail;
    String baseGlob = String.join(".", prefix, differential, caseIdentifier);
    baseGlob = baseGlob + catalystSuffix;

    String glob = "glob:" + Paths.get(directory, baseGlob).toString() + "*.json";

    final PathMatcher pathMatcher = FileSystems.getDefault().getPathMatcher(glob);
    final OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
    final OWLOntologyManager caseManager = OWLManager.createOWLOntologyManager();

    try {
      ReaderDocumentSource readerDocumentSource = new ReaderDocumentSource(
          Files.newBufferedReader(Paths.get(directory, ontoName)));
      OWLOntology owlOntology = manager.loadOntologyFromOntologyDocument(readerDocumentSource);

      ReasonerFactory reasonerFactory = new ReasonerFactory();

      ADHeightStrictPoset botADHeightPoset = new ADHeightStrictPoset(
          new AtomicDecompositionImpl(owlOntology, ModuleType.BOT));
      AxiomSpecificityStrictPoset axiomSpecificityPoset = new AxiomSpecificityStrictPoset(manager,
          owlOntology, reasonerFactory, new SimpleConfiguration(
          timeout_ms));

      List<Strata<OWLAxiom>> strataList = Stream.of(botADHeightPoset, axiomSpecificityPoset)
          .map(StrataFromStrictPoset::new).collect(Collectors.toList());

      Set<OWLDeclarationAxiom> declarations = owlOntology.signature(Imports.INCLUDED)
          .map(entity -> new OWLDeclarationAxiomImpl(entity, Collections.emptySet()))
          .collect(Collectors.toSet());

      Set<OWLAxiom> ontology = owlOntology.logicalAxioms(Imports.INCLUDED)
          .collect(Collectors.toSet());

      Files.walkFileTree(Paths.get(directory), new SimpleFileVisitor<Path>() {

        @Override
        public FileVisitResult visitFile(Path path,
            BasicFileAttributes attrs) throws IOException {
          if (pathMatcher.matches(path)) {
            try {
              process(path, caseManager, ontology, strataList, declarations);
            } catch (Exception e) {
              throw new IOException(e);
            }
          }
          return FileVisitResult.CONTINUE;
        }

        @Override
        public FileVisitResult visitFileFailed(Path file, IOException exc)
            throws IOException {
          return FileVisitResult.CONTINUE;
        }
      });
    } catch (IOException | OWLOntologyCreationException e) {
      return e.getMessage();
    }

    return "OK";
  }

  private static void process(Path path, OWLOntologyManager manager, Set<OWLAxiom> ontology,
      List<Strata<OWLAxiom>> strataList, Set<OWLDeclarationAxiom> declarations)
      throws IOException, OWLOntologyCreationException {

    CaseInstance caseInstance = new CaseInstance(path, manager);
    ReasonerFactory reasonerFactory = new ReasonerFactory();
    Set<OWLEntity> inputSignature = CaseSignatureExtractor
        .signature(caseInstance, reasonerFactory);
    IntegrityConstraintsViolationChecker checker = new IntegrityConstraintsViolationChecker(
        reasonerFactory,
        new SimpleConfiguration(new NullReasonerProgressMonitor(), FreshEntityPolicy.ALLOW,
            timeout_ms, IndividualNodeSetPolicy.BY_NAME),
        caseInstance, declarations);

    BlackBoxMaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
        new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());

    String sizes = strataList.stream().map(strata ->
        measure(maxNonBuilder, strata, ontology, checker, manager, inputSignature))
        .collect(Collectors.joining(","));
    System.out.println(path.getFileName().toString() + "," + sizes);
    manager.removeOntology(caseInstance.getModifiedOntology());
  }

  private static String measure(MaxNonBuilder maxNonBuilder, Strata<OWLAxiom> strata,
      Set<OWLAxiom> ontology, OntologyPropertyChecker checker, OWLOntologyManager manager,
      Set<OWLEntity> inputSignature) {
    MaxNonsBuilder maxNonsBuilder = new StratifiedModMaxNonsBuilder(maxNonBuilder, HSQueue::new,
        strata, manager, inputSignature);

    RepairResult<OWLAxiom> result = maxNonsBuilder.maxNons(ontology, checker);
    return result.getNodes().size() + "," + result.getPaths().size();
  }

}