/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.hst;

import static org.junit.jupiter.api.Assertions.assertTrue;

import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;

public class HittingSetCalculatorTest {

  private static class DummyHittingSetCalculator extends HittingSetCalculator<Integer> {

    final Set<Set<Integer>> availableNodes;

    private DummyHittingSetCalculator(Set<Set<Integer>> allNodes) {
      super(new HSStack<>());
      availableNodes = allNodes;
    }

    @Override
    protected Optional<Set<Integer>> reusable(ImmutableSet<Integer> hittingPath) {
      return Optional.empty();
    }

    @Override
    protected boolean shouldNotTerminate(ImmutableSet<Integer> hittingPath) {
      return true;
    }

    @Override
    protected Optional<Set<Integer>> getNode(ImmutableSet<Integer> hittingPath) {
      return availableNodes.stream().filter(set -> Sets.intersection(hittingPath, set).isEmpty())
          .findAny();
    }

    @Override
    protected void close(ImmutableSet<Integer> hittingPath) {
      closedPaths.add(ImmutableSet.copyOf(hittingPath));
    }

    @Override
    protected Stream<ImmutableSet<Integer>> successors(ImmutableSet<Integer> hittingPath,
        Set<Integer> node) {
      return node.stream().map(Sets::newHashSet).map(set -> Sets.union(hittingPath, set))
          .map(ImmutableSet::copyOf);
    }
  }

  static Stream<Set<Set<Integer>>> availableNodesProvider() {

    return Stream.of(
        Stream.of(
            Sets.newHashSet(2, 3, 5),
            Sets.newHashSet(2, 7, 11),
            Sets.newHashSet(2, 3, 17),
            Sets.newHashSet(7, 19)
        ).collect(Collectors.toSet()),

        Stream.of(
            Sets.newHashSet(2, 3, 5, 7, 11),
            Sets.newHashSet(2, 11, 13),
            Sets.newHashSet(17, 19),
            Sets.newHashSet(5, 19)
        ).collect(Collectors.toSet())
    );
  }

  @ParameterizedTest
  @MethodSource("availableNodesProvider")
  public void resultExactlyAllNodes(Set<Set<Integer>> nodeSet) throws HSTException {
    DummyHittingSetCalculator dummyHittingSetCalculator = new DummyHittingSetCalculator(nodeSet);
    RepairResult repairResult = dummyHittingSetCalculator
        .hittingSet();
    assertTrue(Sets.symmetricDifference(dummyHittingSetCalculator.availableNodes,
        repairResult.getNodes()).isEmpty());
  }

  @ParameterizedTest
  @MethodSource("availableNodesProvider")
  public void finalClosedPathsCutAllNodes(Set<Set<Integer>> nodeSet) throws HSTException {
    DummyHittingSetCalculator dummyHittingSetCalculator = new DummyHittingSetCalculator(nodeSet);
    RepairResult<Integer> repairResult = dummyHittingSetCalculator
        .hittingSet();

    assertTrue(repairResult.getPaths().stream().allMatch(
        cut -> repairResult.getNodes().stream()
            .noneMatch(node -> Sets.intersection(node, cut).isEmpty())));
  }

}
