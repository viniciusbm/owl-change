/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.minimp.full.mod;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import br.usp.ime.owlchange.TestCaseUtils;
import br.usp.ime.owlchange.hst.HSQueue;
import br.usp.ime.owlchange.hst.HSStack;
import br.usp.ime.owlchange.minimp.full.MinImpsBuilderTest;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.BlackBoxMinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.MinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.TrivialMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.ClassicalMinImpShrinker;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.MinImpShrinker;
import br.usp.ime.owlchange.util.TermToAxiomsMapper;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.model.HasSignature;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

class ModuleUpdateMinImpsBuilderTest extends MinImpsBuilderTest {

  @Override
  protected void emptyOntologyNoMinImpForTautology() throws OWLOntologyCreationException {
    MinImpBuilder mockedMinImpBuilder = mock(MinImpBuilder.class);
    when(mockedMinImpBuilder.minImp(any(), any())).thenReturn(
        java.util.Optional.of(Sets.newHashSet()));

    ModuleUpdateMinImpsBuilder minImpsBuilder = new ModuleUpdateMinImpsBuilder(
        mockedMinImpBuilder, HSStack::new, MinImpsBuilderTest.manager, Collections.emptySet());

    emptyOntologyNoMinImpForTautology(minImpsBuilder);
  }

  private static Stream<Arguments> ontologyAndAnySubsumptionProvider() {
    return Stream.of("accounts.owl")
        .map(path -> TestCaseUtils.loadOntology(MinImpsBuilderTest.manager, path)).map(
            ontology -> Arguments.of(
                ontology.axioms().collect(Collectors.toSet()),
                Sets.newHashSet(TestCaseUtils.getAnySubsumption(ontology)))
        );
  }

  @Override
  @ParameterizedTest
  @MethodSource("ontologyAndAnySubsumptionProvider")
  protected void elementsAreMinImps(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException {
    MinImpEnlarger minImpEnlarger = new TrivialMinImpEnlarger();
    MinImpShrinker minImpShrinker = new ClassicalMinImpShrinker();

    Set<OWLEntity> signature = entailments.stream().flatMap(HasSignature::unsortedSignature)
        .collect(Collectors.toSet());

    TermToAxiomsMapper mockedMapper = mock(TermToAxiomsMapper.class);
    when(mockedMapper.termToAxioms(any(OWLEntity.class))).thenReturn(ontology);

    BlackBoxMinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(minImpEnlarger,
        minImpShrinker);
    ModuleUpdateMinImpsBuilder minImpsBuilder = new ModuleUpdateMinImpsBuilder(minImpBuilder,
        HSQueue::new, MinImpsBuilderTest.manager, signature);
    elementsAreMinImps(minImpsBuilder, ontology, entailments);
  }

  @Override
  @ParameterizedTest
  @MethodSource("ontologyAndAnySubsumptionProvider")
  protected void completeMinImps(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException {
    MinImpEnlarger minImpEnlarger = new TrivialMinImpEnlarger();
    MinImpShrinker minImpShrinker = new ClassicalMinImpShrinker();

    Set<OWLEntity> signature = entailments.stream().flatMap(HasSignature::unsortedSignature)
        .collect(Collectors.toSet());

    BlackBoxMinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(minImpEnlarger,
        minImpShrinker);
    ModuleUpdateMinImpsBuilder minImpsBuilder = new ModuleUpdateMinImpsBuilder(minImpBuilder,
        HSQueue::new, MinImpsBuilderTest.manager, signature);
    elementsAreMinImps(minImpsBuilder, ontology, entailments);
  }

  @Disabled
  @Override
  protected void doesNotModifyInputOntology() throws OWLOntologyCreationException {
    // TODO: implement this
  }
}