/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.minimp.single.blackbox.enlarge;

import br.usp.ime.owlchange.TestCaseUtils;
import com.google.common.collect.Sets;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class DivideAndConquerMinImpEnlargerTest extends MinImpEnlargerTest {

  @Override
  @Test
  void allEmptyReturnEmpty() {
    MinImpEnlarger minImpEnlarger = new DivideAndConquerMinImpEnlarger();
    allEmptyReturnEmpty(minImpEnlarger);
  }

  @Override
  @Test
  void emptyOntologyInconsistentFormulaReturnsNull() {
    MinImpEnlarger minImpEnlarger = new DivideAndConquerMinImpEnlarger();
    emptyOntologyInconsistentFormulaReturnsNull(minImpEnlarger);
  }

  private static Stream<Arguments> ontologyAndAnySubsumptionProvider() {
    return Stream.of("accounts.owl")
        .map(path -> TestCaseUtils.loadOntology(ClassicalMinImpEnlargerTest.manager, path)).map(
            ontology -> Arguments.of(
                ontology.axioms().collect(Collectors.toSet()),
                Sets.newHashSet(TestCaseUtils.getAnySubsumption(ontology)))
        );
  }

  @Override
  @ParameterizedTest
  @MethodSource("ontologyAndAnySubsumptionProvider")
  void producesEntailingSubsets(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException {
    MinImpEnlarger minImpEnlarger = new DivideAndConquerMinImpEnlarger();
    producesEntailingSubsets(minImpEnlarger, ontology, entailments);
  }
}
