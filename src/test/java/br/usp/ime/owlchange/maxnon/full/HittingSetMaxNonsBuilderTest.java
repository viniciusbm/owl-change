/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.maxnon.full;

import br.usp.ime.owlchange.TestCaseUtils;
import br.usp.ime.owlchange.hst.HSQueue;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.BlackBoxMaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.ClassicalMaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.MaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.ClassicalMaxNonShrinker;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.MaxNonShrinker;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.TrivialMaxNonShrinker;
import com.google.common.collect.Sets;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

public class HittingSetMaxNonsBuilderTest extends MaxNonsBuilderTest {

  private static Stream<Arguments> ontologyAndAnySubsumptionProvider() {
    return Stream.of("accounts.owl")
        .map(path -> TestCaseUtils.loadOntology(MaxNonsBuilderTest.manager, path)).map(
            ontology -> Arguments.of(
                ontology.axioms().collect(Collectors.toSet()),
                Sets.newHashSet(TestCaseUtils.getAnySubsumption(ontology)))
        );
  }

  @Override
  @ParameterizedTest
  @MethodSource("ontologyAndAnySubsumptionProvider")
  public void elementsAreMaxNons(Set<OWLAxiom> ontology, Set<OWLAxiom> entailments)
      throws OWLOntologyCreationException {
    MaxNonShrinker maxNonShrinker = new TrivialMaxNonShrinker();
    MaxNonEnlarger maxNonEnlarger = new ClassicalMaxNonEnlarger();

    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
        maxNonShrinker, maxNonEnlarger);
    MaxNonsBuilder maxNonsBuilder = new HittingSetMaxNonsBuilder(
        maxNonBuilder, HSQueue::new);

    elementsAreMaxNons(maxNonsBuilder, ontology, entailments);
  }

  @Override
  @Test
  protected void doesNotModifyInputOntology() throws OWLOntologyCreationException {
    MaxNonShrinker maxNonShrinker = new ClassicalMaxNonShrinker();
    MaxNonEnlarger maxNonEnlarger = new ClassicalMaxNonEnlarger();

    HittingSetMaxNonsBuilder maxNonsBuilder = new HittingSetMaxNonsBuilder(
        new BlackBoxMaxNonBuilder(maxNonShrinker, maxNonEnlarger), HSQueue::new);
    doesNotModifyInputOntology(maxNonsBuilder);
  }
}
