#!/bin/bash

fullfile=$1
n_workers=$2
warmup_size=$3


if [ $# -ne 3 ]; then 
    echo "Illegal number of parameters. Required:"
    echo "runs filename/path"
    echo "number of workers"
    echo "number of warmup runs"
    exit 1
fi

if (($2 > 10)); then
    echo "This script only allows up to 10 workers!"
    exit 2
fi

filename=$(basename -- "$fullfile")
filename="${filename%.*}"

shuf "$fullfile" > "${filename}"_shuffled.txt
lines=$(wc -l "$fullfile" | cut -d' ' -f1)
d=$(($lines / $n_workers))
split -d -a 1 -l "$d" "${filename}_shuffled.txt" "$filename"

for (( c=0; c<$2; c++ ))
do
    shuf -n "$warmup_size" "$fullfile" > "${filename}_warmup${c}"
done
