#!/bin/bash

echo "Don't forget to setup the kernel options at boot with Grub"
echo "/proc/cmdline:"
cat /proc/cmdline
echo ""
# https://exablaze.com/docs/exanic/user-guide/benchmarking/benchmarking/
#GRUB_CMDLINE_LINUX_ADDS="isolcpus=2,3,6,7 nohz_full=2,3,6,7 rcu_nocbs=2,3,6,7 intel_idle.max_cstate=0 irqaffinity=0,1,4,5 selinux=0 audit=0 tsc=reliable nmi_watchdog=0 nowatchdog nosoftlockup"
echo "Also, change this file to match the architecture of the computer"

"Disable realtime bandwidth reservation"
echo -1 > /proc/sys/kernel/sched_rt_runtime_us
"Disable watchdog"
echo 0 > /proc/sys/kernel/watchdog
echo 0 > /proc/sys/kernel/nmi_watchdog
# Disable turbo boost
echo 1 > /sys/devices/system/cpu/intel_pstate/no_turbo
echo 0 > /sys/devices/system/cpu/cpufreq/boost

# Turn off one virtual core per physical one
echo 0 > /sys/devices/system/cpu/cpu24/online
echo 0 > /sys/devices/system/cpu/cpu25/online
echo 0 > /sys/devices/system/cpu/cpu26/online
echo 0 > /sys/devices/system/cpu/cpu27/online
echo 0 > /sys/devices/system/cpu/cpu28/online
echo 0 > /sys/devices/system/cpu/cpu29/online
echo 0 > /sys/devices/system/cpu/cpu30/online
echo 0 > /sys/devices/system/cpu/cpu31/online
echo 0 > /sys/devices/system/cpu/cpu32/online
echo 0 > /sys/devices/system/cpu/cpu33/online
echo 0 > /sys/devices/system/cpu/cpu34/online
echo 0 > /sys/devices/system/cpu/cpu35/online
echo 0 > /sys/devices/system/cpu/cpu36/online
echo 0 > /sys/devices/system/cpu/cpu37/online
echo 0 > /sys/devices/system/cpu/cpu38/online
echo 0 > /sys/devices/system/cpu/cpu39/online
echo 0 > /sys/devices/system/cpu/cpu40/online
echo 0 > /sys/devices/system/cpu/cpu41/online
echo 0 > /sys/devices/system/cpu/cpu42/online
echo 0 > /sys/devices/system/cpu/cpu43/online
echo 0 > /sys/devices/system/cpu/cpu44/online
echo 0 > /sys/devices/system/cpu/cpu45/online
echo 0 > /sys/devices/system/cpu/cpu46/online
echo 0 > /sys/devices/system/cpu/cpu47/online


# CPUs where benchmarks will run
cpus="4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23"
# Desired frequency (base frequency, no boost)
frequency=2000000
# SMP affinity mask: flags the cores that will handle interruptions 
# e.g. 3 -> 0011 means that core 0 and 1 will do that
#      0f  -> 1111 means that it will be cores 0 through 3
smp_affinity_mask=0f

# Define which CPUs will handle interrupts
echo $smp_affinity_mask > /proc/irq/default_smp_affinity 

for irq in /proc/irq/*/; do echo "${irq}"; done
for irq in /proc/irq/*/; do
    echo $smp_affinity_mask > "${irq}"smp_affinity;
done

# Check the list
for irq in /proc/irq/*/; do echo -n "$irq  ";  cat "${irq}"smp_affinity_list; done


#gov="performance"
gov="userspace"

# Set benchmark cores for constant frequency
# Also disable machine check thay may case jitter
for cpu in $cpus
do
    echo $gov > /sys/devices/system/cpu/cpu"$cpu"/cpufreq/scaling_governor
    echo 0 >/sys/devices/system/machinecheck/machinecheck"$cpu"/check_interval
done

cpulist=$(echo "$cpus" | tr ' ' ,)

#cpupower -c "$cpulist" frequency-set --max $frequency --min $frequency --governor performance
cpupower -c "$cpulist" frequency-set --max $frequency --min $frequency --governor $gov

# Drop caches
echo 3 | sudo tee /proc/sys/vm/drop_caches
sync
