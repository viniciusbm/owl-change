#!/bin/bash

toWarmup=120
tout=480

TEST=false

if [ $# -ne 4 ]; then 
    echo "Illegal number of parameters. Required:"
    echo "fileprefix"
    echo "id of this worker"
    echo "comma-separated cpu-list"
    echo "the total number of workers"
    exit 1
fi

fileprefix=$1
id=$2
cpus=$3
total_workers=$4


wresults_dir="../../${fileprefix}_wbresults"
wlogs_dir="../../${fileprefix}_wblogs"
results_dir="../../${fileprefix}_bresults"
logs_dir="../../${fileprefix}_blogs"

parseId() {
    echo "$1" | cut -d, -f 1
}

parseMethod() {
    echo "$1" | cut -d, -f 2
}

parseReasoner() {
    echo "$1" | cut -d, -f 3
}

parseCaseinfo() {
    echo "$1" | cut -d, -f 4
}

parseRepeat() {
    echo "$1" | cut -d, -f 5
}

build_n_run() {
    seq=$(parseId "$1")
    met=$(parseMethod "$1")
    rea=$(parseReasoner "$1")
    inf=$(parseCaseinfo "$1")
    rep=$(parseRepeat "$1")
    if [ "$3" = "warmup" ]; then
        outfile="${wresults_dir}/w${seq}.csv"
        logfile="${wlogs_dir}/w${seq}.log"
    elif [ "$3" = "pad" ]; then
        outfile="${wresults_dir}/p${seq}.csv"
        logfile="${wlogs_dir}/p${seq}.log"
    else
        outfile="${results_dir}/${seq}.csv"
        logfile="${logs_dir}/${seq}.log"
    fi

    toReason=$2
    toReasonMillis=$((toReason*1000))
    toJMH=$((toReason + 60))
    to15=$((toJMH + 15))
    to9=$((to15 + 15))
   
    date
    echo "${3} : ${seq} : ${met} : ${rea} : ${inf} : ${rep}"

    JAVAxOPTS="-Dreasoner=${rea} -DtimeoutMillis=${toReasonMillis}
    -Dcaseinfo=${inf} -Xms2G -Xmx12G -Djmh.ignoreLock=true" JMHxARGS=(-bm ss -wi 0 -i 1 -opi 1 -f 1 -to "${toJMH}s"
                -prof br.usp.ime.owlchange.MeasurementManager -prof perf 
                -v EXTRA -rf CSV -rff "${outfile}" "${met}*")

    echo "JAVA_OPTS=${JAVAxOPTS} cgexec -g memory,cpuset: exp${id} timeout -k ${to9}s ${to15}s nice -n -15 taskset -c $cpus ./owl2dl-change-mod-jmh ${JMHxARGS}"

    if [ "$TEST" = false ]; then
         JAVA_OPTS="${JAVAxOPTS}" cgexec -g "memory,cpuset:exp${id}" timeout -k ${to9}s ${to15}s nice -n -15 taskset -c "$cpus" ./owl2dl-change-mod-jmh "${JMHxARGS[@]}" > "${logfile}" 2>&1
    fi
    echo ""
}

if [ "$TEST" = false ]; then
    mkdir "${wresults_dir}"
    mkdir "${wlogs_dir}"
    mkdir "${results_dir}"
    mkdir "${logs_dir}"
fi

while IFS="" read -r p || [ -n "$p" ]
do
    if [ "$TEST" = true ]; then
        echo "$p"
    fi
    build_n_run "$p" $toWarmup "warmup"
done < "${fileprefix}_warmup${id}"

while IFS="" read -r p || [ -n "$p" ]
do
    if [ "$TEST" = true ]; then
        echo "$p"
    fi
    build_n_run "$p" $tout "run"
done < "${fileprefix}${id}"

echo "DONE $(date)" > ok"${id}".stt

while (($(find . -name "ok[0-9].stt" 2>/dev/null | wc -l) < total_workers))
do
    p=$(shuf -n 1 "${fileprefix}${id}")

    if [ "$TEST" = true ]; then
        echo "$p"
    fi
    build_n_run "$p" $tout "pad"
done
