#!/bin/bash

if [ $# -ne 4 ]; then 
    echo "Illegal number of parameters. Required:"
    echo "casefile"
    echo "methodsfile"
    echo "number of repetitions per combination" 
    echo "starting sequence number" 
    exit 1
fi

casefile=$1
methodfile=$2

reasoners=("JFact" "Hermit")
nruns=$"$3"
seq="$4"

while IFS="" read -u 3 -r p || [ -n "$p" ] <&3
do
    for reasoner in "${reasoners[@]}"; do
        while IFS="" read -u 4 -r method || [ -n "$method" ] <&4
        do
            for (( run=1; run<=nruns; run++ )); do
                echo "${seq},${method},${reasoner},${p},${run}"
                seq=$((seq + 1))
            done
        done 4< "$methodfile"
    done
done 3< "$casefile"
