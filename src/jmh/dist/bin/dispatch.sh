#!/bin/bash

rm ok[0-9].stt

# script to run
base_cmd=./benchmark_runner.sh
# prefix of the splitted run files / run filename without extension
prefix=full

echo "WARNING: Check this file after running to make sure you lauched the
benchmarks for the right experiment."

($base_cmd $prefix 0 4,5 10 >> benchmarkFull0.log &)
($base_cmd $prefix 1 6,7 10 >> benchmarkFull1.log &)
($base_cmd $prefix 2 8,9 10 >> benchmarkFull2.log &)
($base_cmd $prefix 3 10,11 10 >> benchmarkFull3.log &)
($base_cmd $prefix 4 12,13 10 >> benchmarkFull4.log &)
($base_cmd $prefix 5 14,15 10 >> benchmarkFull5.log &)
($base_cmd $prefix 6 16,17 10 >> benchmarkFull6.log &)
($base_cmd $prefix 7 18,19 10 >> benchmarkFull7.log &)
($base_cmd $prefix 8 20,21 10 >> benchmarkFull8.log &)
($base_cmd $prefix 9 22,23 10 >> benchmarkFull9.log &)

wait
echo $(date) > launch.date
echo "Everyone dismissed!"
