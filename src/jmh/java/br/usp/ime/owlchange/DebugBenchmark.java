/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange;

import br.usp.ime.owl2dlccc.CaseInstance;
import br.usp.ime.owlchange.hst.HSQueue;
import br.usp.ime.owlchange.hst.HSStack;
import br.usp.ime.owlchange.maxnon.full.HittingSetMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.MaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.MaxNonsSanitiser;
import br.usp.ime.owlchange.maxnon.full.early.SingleNodeDefaultMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.early.SingleNodeSMEMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.mod.ModuleUpdateMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.mod.SingleMEMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonValidator;
import br.usp.ime.owlchange.maxnon.single.blackbox.BlackBoxMaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.ClassicalMaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.SyntacticConnectivityMaxNonShrinker;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.TrivialMaxNonShrinker;
import br.usp.ime.owlchange.minimp.full.HittingSetMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.MinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.MinImpsSanitiser;
import br.usp.ime.owlchange.minimp.full.early.SinglePathDefaultMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.early.SinglePathMUMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.early.SinglePathSMEMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.mod.ModuleUpdateMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.mod.SingleMEMinImpsBuilder;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import br.usp.ime.owlchange.minimp.single.MinImpValidator;
import br.usp.ime.owlchange.minimp.single.blackbox.BlackBoxMinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.SyntacticConnectivityMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.TrivialMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.Horridge2011DivideAndConquerMinImpShrinker;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.FreshEntityPolicy;
import org.semanticweb.owlapi.reasoner.IndividualNodeSetPolicy;
import org.semanticweb.owlapi.reasoner.NullReasonerProgressMonitor;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import uk.ac.manchester.cs.jfact.JFactFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLDeclarationAxiomImpl;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

public class DebugBenchmark {

  @State(Scope.Thread)
  public static class DebugState {

    private OWLOntology owlOntology;
    private Set<OWLAxiom> ontology;
    private IntegrityConstraintsViolationChecker checker;
    private Set<OWLEntity> inputSignature;
    private OWLOntologyManager manager;

    private OWLReasonerFactory factorySelector(String reasoner) {
      if (reasoner.equalsIgnoreCase("HermiT")) {
        return new ReasonerFactory();
      } else {
        return new JFactFactory();
      }
    }

    public DebugState() {
      try {
        Path path = Paths.get(System.getProperty("caseinfo"));
        String reasoner = System.getProperty("reasoner", "JFact");
        long timeout = Long.getLong("timeoutMillis", 0L);
        this.manager = OWLManager.createOWLOntologyManager();
        CaseInstance caseInstance = new CaseInstance(path, manager);

        this.owlOntology = caseInstance.getModifiedOntology();
        this.inputSignature = CaseSignatureExtractor
            .signature(caseInstance, factorySelector(reasoner));
        Set<OWLDeclarationAxiom> declarations = this.owlOntology.signature(Imports.INCLUDED)
            .map(entity -> new OWLDeclarationAxiomImpl(entity,
                Collections.emptySet())).collect(Collectors.toSet());
        this.checker = new IntegrityConstraintsViolationChecker(factorySelector(reasoner),
            new SimpleConfiguration(new NullReasonerProgressMonitor(), FreshEntityPolicy.ALLOW,
                timeout, IndividualNodeSetPolicy.BY_NAME),
            caseInstance, declarations);

        this.ontology = this.owlOntology.logicalAxioms(Imports.INCLUDED)
            .collect(Collectors.toSet());
      } catch (OWLOntologyCreationException | IOException e) {
        throw new RuntimeException(e);
      }
    }

    public Set<OWLAxiom> getOntology() {
      return ontology;
    }

    public IntegrityConstraintsViolationChecker getChecker() {
      return checker;
    }

    public Set<OWLEntity> getInputSignature() {
      return inputSignature;
    }

    public OWLOntologyManager getManager() {
      return manager;
    }

    public OWLOntology getOWLOntology() {
      return owlOntology;
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void debug_maxNons(DebugState state) {
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
        new SyntacticConnectivityMaxNonShrinker(state.getInputSignature()),
        new ClassicalMaxNonEnlarger());
    MaxNonsBuilder maxNonsBuilder = new HittingSetMaxNonsBuilder(maxNonBuilder, HSQueue::new);
    Set<Set<OWLAxiom>> basicMaxNons = maxNonsBuilder
        .maxNons(state.getOntology(), state.getChecker()).getNodes();

    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MaxNonBuilder smeMaxNonBuilder = new BlackBoxMaxNonBuilder(new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());
    MaxNonsBuilder smeMaxNonsBuilder = new SingleMEMaxNonsBuilder(smeMaxNonBuilder, HSQueue::new,
        moduleExtractor, state.getInputSignature());
    Set<Set<OWLAxiom>> smeMaxNons = smeMaxNonsBuilder
        .maxNons(state.getOntology(), state.getChecker()).getNodes();

    MaxNonBuilder muMaxNonBuilder = new BlackBoxMaxNonBuilder(new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());
    MaxNonsBuilder muMaxNonsBuilder = new ModuleUpdateMaxNonsBuilder(muMaxNonBuilder, HSQueue::new,
        state.getManager(), state.inputSignature);
    Set<Set<OWLAxiom>> muMaxNons = muMaxNonsBuilder
        .maxNons(state.getOntology(), state.getChecker()).getNodes();

    if (!muMaxNons.equals(smeMaxNons) || !muMaxNons.equals(basicMaxNons)) {
      throw new RuntimeException("NOT EQUAL");
    }

    if (!MaxNonsSanitiser.sanitise(basicMaxNons, state.getOntology(), state.getChecker())) {
      throw new RuntimeException("Basic MaxNons failed");
    }

    if (!MaxNonsSanitiser.sanitise(smeMaxNons, state.getOntology(), state.getChecker())) {
      throw new RuntimeException("SME MaxNons failed");
    }

    if (!MaxNonsSanitiser.sanitise(muMaxNons, state.getOntology(), state.getChecker())) {
      throw new RuntimeException("MU MaxNons failed");
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void debug_minImps(DebugState state) {
    MinImpBuilder basicMinImpBuilder = new BlackBoxMinImpBuilder(
        new SyntacticConnectivityMinImpEnlarger(state.getInputSignature()),
        new Horridge2011DivideAndConquerMinImpShrinker());
    HittingSetMinImpsBuilder basicMinImpsBuilder = new HittingSetMinImpsBuilder(basicMinImpBuilder,
        HSQueue::new);
    Set<Set<OWLAxiom>> basicMinImps = basicMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();

    MinImpBuilder smeMinImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MinImpsBuilder smeMinImpsBuilder = new SingleMEMinImpsBuilder(smeMinImpBuilder, HSQueue::new,
        moduleExtractor, state.getInputSignature());
    Set<Set<OWLAxiom>> smeMinImps = smeMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();

    MinImpBuilder muMinImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    MinImpsBuilder muMinImpsBuilder = new ModuleUpdateMinImpsBuilder(muMinImpBuilder, HSQueue::new,
        state.getManager(), state.inputSignature);
    Set<Set<OWLAxiom>> muMinImps = muMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();

    if (!muMinImps.equals(smeMinImps) || !muMinImps.equals(basicMinImps)) {
      throw new RuntimeException("NOT EQUAL");
    }

    if (!MinImpsSanitiser.sanitise(basicMinImps, state.getOntology(), state.getChecker())) {
      throw new RuntimeException("Basic MinImps failed");
    }

    if (!MinImpsSanitiser.sanitise(smeMinImps, state.getOntology(), state.getChecker())) {
      throw new RuntimeException("SME MinImps failed");
    }

    if (!MinImpsSanitiser.sanitise(muMinImps, state.getOntology(), state.getChecker())) {
      throw new RuntimeException("MU MinImps failed");
    }
  }

  // Single Using Queue

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void debug_singleMaxNon(DebugState state) {
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
        new SyntacticConnectivityMaxNonShrinker(state.getInputSignature()),
        new ClassicalMaxNonEnlarger());
    MaxNonsBuilder queueBasicMaxNonsBuilder = new SingleNodeDefaultMaxNonsBuilder(maxNonBuilder,
        HSQueue::new);
    MaxNonsBuilder stackBasicMaxNonsBuilder = new SingleNodeDefaultMaxNonsBuilder(maxNonBuilder,
        HSStack::new);

    MaxNonBuilder smeMaxNonBuilder = new BlackBoxMaxNonBuilder(new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MaxNonsBuilder queueSMEMaxNonsBuilder = new SingleNodeSMEMaxNonsBuilder(smeMaxNonBuilder,
        HSQueue::new,
        moduleExtractor, state.getInputSignature());
    MaxNonsBuilder stackSMEMaxNonsBuilder = new SingleNodeSMEMaxNonsBuilder(smeMaxNonBuilder,
        HSStack::new,
        moduleExtractor, state.getInputSignature());

    Set<Set<OWLAxiom>> queueBasicMaxNons = queueBasicMaxNonsBuilder
        .maxNons(state.getOntology(), state.getChecker()).getNodes();
    Set<Set<OWLAxiom>> stackBasicMaxNons = stackBasicMaxNonsBuilder
        .maxNons(state.getOntology(), state.getChecker()).getNodes();
    Set<Set<OWLAxiom>> queueSMEMaxNons = queueSMEMaxNonsBuilder
        .maxNons(state.getOntology(), state.getChecker()).getNodes();
    Set<Set<OWLAxiom>> stackSMEMaxNons = stackSMEMaxNonsBuilder
        .maxNons(state.getOntology(), state.getChecker()).getNodes();

    boolean ok = Stream.of(queueBasicMaxNons, stackBasicMaxNons, queueSMEMaxNons, stackSMEMaxNons)
        .allMatch(x -> x.stream()
            .allMatch(y -> MaxNonValidator.isMaxNon(y, state.getOntology(), state.getChecker())));

    if (!ok) {
      throw new RuntimeException("Someone failed");
    }
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void debug_singleMinImp(DebugState state) {
    MinImpBuilder basicminImpBuilder = new BlackBoxMinImpBuilder(
        new SyntacticConnectivityMinImpEnlarger(state.getInputSignature()),
        new Horridge2011DivideAndConquerMinImpShrinker());
    MinImpsBuilder queueBasicMinImpsBuilder = new SinglePathDefaultMinImpsBuilder(
        basicminImpBuilder, HSQueue::new);
    MinImpsBuilder stackBasicMinImpsBuilder = new SinglePathDefaultMinImpsBuilder(
        basicminImpBuilder, HSStack::new);

    MinImpBuilder smeMinImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MinImpsBuilder queueSMEMinImpsBuilder = new SinglePathSMEMinImpsBuilder(smeMinImpBuilder,
        HSQueue::new,
        moduleExtractor, state.getInputSignature());
    MinImpsBuilder stackSMEMinImpsBuilder = new SinglePathSMEMinImpsBuilder(smeMinImpBuilder,
        HSStack::new,
        moduleExtractor, state.getInputSignature());

    MinImpBuilder muMinImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    MinImpsBuilder queueMUMinImpsBuilder = new SinglePathMUMinImpsBuilder(muMinImpBuilder,
        HSQueue::new,
        state.getManager(), state.inputSignature);
    MinImpsBuilder stackMUMinImpsBuilder = new SinglePathMUMinImpsBuilder(muMinImpBuilder,
        HSStack::new,
        state.getManager(), state.inputSignature);

    Set<Set<OWLAxiom>> queueMUMinImps = queueMUMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();
    Set<Set<OWLAxiom>> stackMUMinImps = stackMUMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();

    Set<Set<OWLAxiom>> queueSMEMinImps = queueSMEMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();
    Set<Set<OWLAxiom>> stackSMEMinImps = stackSMEMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();

    Set<Set<OWLAxiom>> queueBasicMinImps = queueBasicMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();
    Set<Set<OWLAxiom>> stackBasicMinImps = stackBasicMinImpsBuilder
        .minImps(state.getOntology(), state.getChecker()).getNodes();

    boolean ok = Stream
        .of(queueBasicMinImps, stackBasicMinImps, queueSMEMinImps, stackSMEMinImps, queueMUMinImps,
            stackMUMinImps).allMatch(x -> x.stream()
            .allMatch(y -> MinImpValidator.isMinImp(y, state.getOntology(), state.getChecker())));

    if (!ok) {
      throw new RuntimeException("Someone failed");
    }
  }
}
