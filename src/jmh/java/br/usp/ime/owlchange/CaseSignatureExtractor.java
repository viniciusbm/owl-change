/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange;

import br.usp.ime.owl2dlccc.CaseInstance;
import com.google.common.collect.Sets;
import java.util.Set;
import org.semanticweb.owlapi.model.HasSignature;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;

public class CaseSignatureExtractor {

  public static Set<OWLEntity> signature(CaseInstance caseInstance,
      OWLReasonerFactory reasonerFactory) {
    Set<OWLEntity> signature = Sets.newHashSet();
    caseInstance.getPositives().stream().flatMap(HasSignature::unsortedSignature)
        .forEach(signature::add);
    caseInstance.getNegatives().stream().flatMap(HasSignature::unsortedSignature)
        .forEach(signature::add);
    if (caseInstance.getEnforceCoherence().isPresent()) {
      OWLReasoner reasoner = reasonerFactory.createReasoner(caseInstance.getModifiedOntology());
      try {
        reasoner.unsatisfiableClasses().forEach(signature::add);
      } catch (InconsistentOntologyException e) {
        caseInstance.getModifiedOntology().classesInSignature(Imports.INCLUDED)
            .forEach(signature::add);
      }
      reasoner.dispose();
    }
    return signature;
  }
}
