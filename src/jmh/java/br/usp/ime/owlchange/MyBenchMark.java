/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange;

import br.usp.ime.owl2dlccc.CaseInstance;
import br.usp.ime.owlchange.hst.HSQueue;
import br.usp.ime.owlchange.hst.HSStack;
import br.usp.ime.owlchange.maxnon.full.HittingSetMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.MaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.early.SingleNodeDefaultMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.early.SingleNodeSMEMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.mod.ModuleUpdateMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.mod.SingleMEMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.BlackBoxMaxNonBuilder;
import br.usp.ime.owlchange.maxnon.single.blackbox.enlarge.ClassicalMaxNonEnlarger;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.SyntacticConnectivityMaxNonShrinker;
import br.usp.ime.owlchange.maxnon.single.blackbox.shrink.TrivialMaxNonShrinker;
import br.usp.ime.owlchange.minimp.full.HittingSetMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.MinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.early.SinglePathDefaultMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.early.SinglePathMUMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.early.SinglePathSMEMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.mod.ModuleUpdateMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.mod.SingleMEMinImpsBuilder;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.BlackBoxMinImpBuilder;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.SyntacticConnectivityMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.enlarge.TrivialMinImpEnlarger;
import br.usp.ime.owlchange.minimp.single.blackbox.shrink.Horridge2011DivideAndConquerMinImpShrinker;
import br.usp.ime.owlchange.util.InternalsTermToAxiomsMapper;
import br.usp.ime.owlchange.util.TermToAxiomsMapper;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.semanticweb.HermiT.ReasonerFactory;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.FreshEntityPolicy;
import org.semanticweb.owlapi.reasoner.IndividualNodeSetPolicy;
import org.semanticweb.owlapi.reasoner.NullReasonerProgressMonitor;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import uk.ac.manchester.cs.jfact.JFactFactory;
import uk.ac.manchester.cs.owl.owlapi.OWLDeclarationAxiomImpl;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

public class MyBenchMark {

  @State(Scope.Thread)
  public static class MyBenchmarkState {

    private OWLOntology owlOntology;
    private Set<OWLAxiom> ontology;
    private IntegrityConstraintsViolationChecker checker;
    private Set<OWLEntity> inputSignature;
    private OWLOntologyManager manager;

    private OWLReasonerFactory factorySelector(String reasoner) {
      if (reasoner.equalsIgnoreCase("HermiT")) {
        return new ReasonerFactory();
      } else {
        return new JFactFactory();
      }
    }

    public MyBenchmarkState() {
      try {
        Path path = Paths.get(System.getProperty("caseinfo"));
        String reasoner = System.getProperty("reasoner", "JFact");
        long timeout = Long.getLong("timeoutMillis", 0L);
        this.manager = OWLManager.createOWLOntologyManager();
        CaseInstance caseInstance = new CaseInstance(path, manager);

        this.owlOntology = caseInstance.getModifiedOntology();
        this.inputSignature = CaseSignatureExtractor
            .signature(caseInstance, factorySelector(reasoner));
        Set<OWLDeclarationAxiom> declarations = this.owlOntology.signature(Imports.INCLUDED)
            .map(entity -> new OWLDeclarationAxiomImpl(entity,
                Collections.emptySet())).collect(Collectors.toSet());
        this.checker = new IntegrityConstraintsViolationChecker(factorySelector(reasoner),
            new SimpleConfiguration(new NullReasonerProgressMonitor(), FreshEntityPolicy.ALLOW,
                timeout, IndividualNodeSetPolicy.BY_NAME),
            caseInstance, declarations);

        this.ontology = this.owlOntology.logicalAxioms(Imports.INCLUDED)
            .collect(Collectors.toSet());
      } catch (OWLOntologyCreationException | IOException e) {
        throw new RuntimeException(e);
      }
    }

    public Set<OWLAxiom> getOntology() {
      return ontology;
    }

    public IntegrityConstraintsViolationChecker getChecker() {
      return checker;
    }

    public Set<OWLEntity> getInputSignature() {
      return inputSignature;
    }

    public OWLOntologyManager getManager() {
      return manager;
    }

    public OWLOntology getOWLOntology() {
      return owlOntology;
    }
  }

  private void finishBenchmark(Set<Set<OWLAxiom>> sets,
      IntegrityConstraintsViolationChecker checker) {
    MeasurementManager.setReturnValue(sets);
    MeasurementManager.setResult("calls", checker.getCalls(), "x");
    MeasurementManager.tag("exit");
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void basicMaxNons(MyBenchmarkState state) {
    MeasurementManager.reset();
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
        new SyntacticConnectivityMaxNonShrinker(state.getInputSignature()),
        new ClassicalMaxNonEnlarger());
    MaxNonsBuilder maxNonsBuilder = new HittingSetMaxNonsBuilder(maxNonBuilder, HSQueue::new);
    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(maxNons, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void sMEMaxNons(MyBenchmarkState state) {
    MeasurementManager.reset();
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MaxNonsBuilder maxNonsBuilder = new SingleMEMaxNonsBuilder(maxNonBuilder, HSQueue::new,
        moduleExtractor, state.getInputSignature());
    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(maxNons, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void moduleUpdateMaxNons(MyBenchmarkState state) {
    MeasurementManager.reset();
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());
    TermToAxiomsMapper graph = new InternalsTermToAxiomsMapper(state.getOWLOntology());
    MaxNonsBuilder maxNonsBuilder = new ModuleUpdateMaxNonsBuilder(maxNonBuilder, HSQueue::new,
        state.getManager(), state.inputSignature);
    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(maxNons, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void basicMinImps(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(
        new SyntacticConnectivityMinImpEnlarger(state.getInputSignature()),
        new Horridge2011DivideAndConquerMinImpShrinker());
    HittingSetMinImpsBuilder minImpsBuilder = new HittingSetMinImpsBuilder(minImpBuilder,
        HSQueue::new);
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void sMEMinImps(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MinImpsBuilder minImpsBuilder = new SingleMEMinImpsBuilder(minImpBuilder, HSQueue::new,
        moduleExtractor, state.getInputSignature());
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void moduleUpdateMinImps(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    TermToAxiomsMapper graph = new InternalsTermToAxiomsMapper(state.getOWLOntology());
    MinImpsBuilder minImpsBuilder = new ModuleUpdateMinImpsBuilder(minImpBuilder, HSQueue::new,
        state.getManager(), state.inputSignature);
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  // Single Using Queue

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singleNodeBasicMaxNonsQueue(MyBenchmarkState state) {
    MeasurementManager.reset();
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
        new SyntacticConnectivityMaxNonShrinker(state.getInputSignature()),
        new ClassicalMaxNonEnlarger());
    MaxNonsBuilder maxNonsBuilder = new SingleNodeDefaultMaxNonsBuilder(maxNonBuilder,
        HSQueue::new);
    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(maxNons, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singleNodeSMEMaxNonsQueue(MyBenchmarkState state) {
    MeasurementManager.reset();
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MaxNonsBuilder maxNonsBuilder = new SingleNodeSMEMaxNonsBuilder(maxNonBuilder, HSQueue::new,
        moduleExtractor,
        state.getInputSignature());
    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(maxNons, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singlePathBasicMinImpsQueue(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(
        new SyntacticConnectivityMinImpEnlarger(state.getInputSignature()),
        new Horridge2011DivideAndConquerMinImpShrinker());
    MinImpsBuilder minImpsBuilder = new SinglePathDefaultMinImpsBuilder(minImpBuilder,
        HSQueue::new);
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singlePathSMEMinImpsQueue(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MinImpsBuilder minImpsBuilder = new SinglePathSMEMinImpsBuilder(minImpBuilder, HSQueue::new,
        moduleExtractor, state.getInputSignature());
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singlePathModuleUpdateMinImpsQueue(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    MinImpsBuilder minImpsBuilder = new SinglePathMUMinImpsBuilder(minImpBuilder, HSQueue::new,
        state.getManager(), state.inputSignature);
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  // Single using Stack

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singleNodeBasicMaxNonsStack(MyBenchmarkState state) {
    MeasurementManager.reset();
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(
        new SyntacticConnectivityMaxNonShrinker(state.getInputSignature()),
        new ClassicalMaxNonEnlarger());
    MaxNonsBuilder maxNonsBuilder = new SingleNodeDefaultMaxNonsBuilder(maxNonBuilder,
        HSStack::new);
    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(maxNons, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singleNodeSMEMaxNonsStack(MyBenchmarkState state) {
    MeasurementManager.reset();
    MaxNonBuilder maxNonBuilder = new BlackBoxMaxNonBuilder(new TrivialMaxNonShrinker(),
        new ClassicalMaxNonEnlarger());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MaxNonsBuilder maxNonsBuilder = new SingleNodeSMEMaxNonsBuilder(maxNonBuilder, HSStack::new,
        moduleExtractor,
        state.getInputSignature());
    Set<Set<OWLAxiom>> maxNons = maxNonsBuilder.maxNons(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(maxNons, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singlePathBasicMinImpsStack(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(
        new SyntacticConnectivityMinImpEnlarger(state.getInputSignature()),
        new Horridge2011DivideAndConquerMinImpShrinker());
    MinImpsBuilder minImpsBuilder = new SinglePathDefaultMinImpsBuilder(minImpBuilder,
        HSStack::new);
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singlePathSMEMinImpsStack(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
        state.getManager(), state.getOntology().stream(), ModuleType.STAR);
    MinImpsBuilder minImpsBuilder = new SinglePathSMEMinImpsBuilder(minImpBuilder, HSStack::new,
        moduleExtractor, state.getInputSignature());
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }

  @Benchmark
  @BenchmarkMode(Mode.SingleShotTime)
  @OutputTimeUnit(TimeUnit.MICROSECONDS)
  public void singlePathModuleUpdateMinImpsStack(MyBenchmarkState state) {
    MeasurementManager.reset();
    MinImpBuilder minImpBuilder = new BlackBoxMinImpBuilder(new TrivialMinImpEnlarger(),
        new Horridge2011DivideAndConquerMinImpShrinker());
    MinImpsBuilder minImpsBuilder = new SinglePathMUMinImpsBuilder(minImpBuilder, HSStack::new,
        state.getManager(), state.inputSignature);
    Set<Set<OWLAxiom>> minImps = minImpsBuilder.minImps(state.getOntology(), state.getChecker())
        .getNodes();
    finishBenchmark(minImps, state.checker);
  }
}
