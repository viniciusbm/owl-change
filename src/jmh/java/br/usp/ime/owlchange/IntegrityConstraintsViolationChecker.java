/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange;

import br.usp.ime.owl2dlccc.CaseInstance;
import com.google.common.collect.ImmutableSet;
import java.util.Collections;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.HasSignature;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLDeclarationAxiom;
import org.semanticweb.owlapi.model.OWLLogicalAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLRuntimeException;
import org.semanticweb.owlapi.model.parameters.Imports;
import org.semanticweb.owlapi.reasoner.InconsistentOntologyException;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.reasoner.OWLReasonerConfiguration;
import org.semanticweb.owlapi.reasoner.OWLReasonerFactory;
import org.semanticweb.owlapi.reasoner.SimpleConfiguration;
import uk.ac.manchester.cs.owl.owlapi.OWLDeclarationAxiomImpl;

public class IntegrityConstraintsViolationChecker implements OntologyPropertyChecker {

  private static final int DEFAULT_TIMEOUT = 50000;
  protected OWLOntology ontology;
  protected OWLReasoner reasoner;
  private CaseInstance caseInstance;
  private ImmutableSet<OWLAxiom> fixedPart;

  public IntegrityConstraintsViolationChecker(OWLReasonerFactory reasonerFactory,
      CaseInstance caseInstance, Set<OWLDeclarationAxiom> fixedDeclarations)
      throws OWLOntologyCreationException {
    this(reasonerFactory, DEFAULT_TIMEOUT, caseInstance, fixedDeclarations);

  }

  public IntegrityConstraintsViolationChecker(OWLReasonerFactory reasonerFactory, int timeout,
      CaseInstance caseInstance, Set<OWLDeclarationAxiom> fixedDeclarations)
      throws OWLOntologyCreationException {
    this(reasonerFactory, new SimpleConfiguration(timeout), caseInstance, fixedDeclarations);
  }

  public IntegrityConstraintsViolationChecker(OWLReasonerFactory reasonerFactory,
      OWLReasonerConfiguration reasonerConfiguration, CaseInstance caseInstance,
      Set<OWLDeclarationAxiom> fixedDeclarations)
      throws OWLOntologyCreationException {
    this.ontology = manager.createOntology();
    this.reasoner = reasonerFactory.createReasoner(ontology, reasonerConfiguration);
    this.caseInstance = caseInstance;
    this.fixedPart = ImmutableSet.copyOf(caseInstance.getPositives());
    Stream<OWLDeclarationAxiom> positiveDeclarations = caseInstance.getPositives().stream()
        .flatMap(HasSignature::unsortedSignature).map(e -> new OWLDeclarationAxiomImpl(e,
            Collections.emptySet()));
    Stream<OWLDeclarationAxiom> negativeDeclarations = caseInstance.getNegatives().stream()
        .flatMap(HasSignature::unsortedSignature).map(e -> new OWLDeclarationAxiomImpl(e,
            Collections.emptySet()));
    this.ontology.addAxioms(fixedDeclarations);
    this.ontology.addAxioms(positiveDeclarations);
    this.ontology.addAxioms(negativeDeclarations);
    this.ontology.addAxioms(fixedPart);
  }

  protected static OWLOntologyManager manager = OWLManager.createOWLOntologyManager();
  private long calls = 0;

  public Set<OWLAxiom> getAxioms() {
    return ontology.axioms().collect(Collectors.toSet());
  }

  @Override
  public void setAxioms(Set<OWLAxiom> axioms) {
    Stream<OWLLogicalAxiom> toRemove = ontology.logicalAxioms()
        .filter(ax -> !fixedPart.contains(ax) && !axioms.contains(ax));
    ontology.removeAxioms(toRemove);
    ontology.addAxioms(axioms.stream().filter(ax -> !this.ontology.containsAxiom(ax)));
  }

  @Override
  public boolean hasProperty(Set<OWLAxiom> axioms) throws OWLRuntimeException {

    setAxioms(axioms);
    reasoner.flush();
    calls += 1;

    if (caseInstance.getEnforceConsistency().isPresent()) {
      if (caseInstance.getEnforceConsistency().get() != reasoner.isConsistent()) {
        return true;
      }
    }

    if (caseInstance.getEnforceCoherence().isPresent()) {
      try {
        if (caseInstance.getEnforceCoherence().get() != reasoner.unsatisfiableClasses().findAny()
            .isPresent()) {
          return true;
        }
      } catch (InconsistentOntologyException e) {
        boolean coherent = !this.ontology.classesInSignature(Imports.INCLUDED).findAny()
            .isPresent();
        if (caseInstance.getEnforceCoherence().get() != coherent) {
          return true;
        }
      }
    }

    if (!caseInstance.getPositives().isEmpty()) {
      try {
        if (!reasoner.isEntailed(caseInstance.getPositives())) {
          return true;
        }
      } catch (InconsistentOntologyException e) {
        // Do nothing, inconsistency means that all of them are entailed
      }
    }

    if (!caseInstance.getNegatives().isEmpty()) {
      try {
        if (reasoner.isEntailed(caseInstance.getNegatives())) {
          return true;
        }
      } catch (InconsistentOntologyException e) {
        return true;
      }
    }

    return false;
  }

  public long getCalls() {
    return calls;
  }
}
