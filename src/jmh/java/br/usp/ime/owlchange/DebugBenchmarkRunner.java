/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange;

import br.usp.ime.owlchange.DebugBenchmark.DebugState;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DebugBenchmarkRunner {

  private static Logger LOGGER = LoggerFactory.getLogger(DebugBenchmarkRunner.class);

  public static void main(String[] args) throws Exception {
    LOGGER.debug("Starting debug");
    String[] paths = {
        /*"/home/ricardo/casegen/output/pgxo.A.0singleSubClassOfContract_contraction_2.json",
        "/home/ricardo/casegen/output/trans.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/medeon.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/diagont.A.1inconsistentFromDisjointSuper_consistentRevision_2.json",
        "/home/ricardo/casegen/output/medeon.A.0singleSubClassOfContract_contraction_2.json",
        "/home/ricardo/casegen/output/atc.A.0singleSubClassOfContract_contraction_2.json",
        "/home/ricardo/casegen/output/ogi.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/diagont.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/hpio.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/ccon.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/fire.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/doccc.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/medo.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/chembio.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/pseudo.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/vario.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/bco.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/psds.A.1inconsistentFromDisjointSuper_consistentRevision_2.json",
        "/home/ricardo/casegen/output/rao.A.1inconsistentFromDisjointSuper_consistentRevision_3.json",
        "/home/ricardo/casegen/output/repo.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/gfo.A.0singleSubClassOfContract_contraction_2.json",
        "/home/ricardo/casegen/output/ontoma.A.0singleSubClassOfContract_contraction_3.json",
        "/home/ricardo/casegen/output/carelex.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/typon.A.1inconsistentFromDisjointSuper_consistentRevision_2.json",
        "/home/ricardo/casegen/output/spto.A.1inconsistentFromDisjointSuper_consistentRevision_3.json",
        "/home/ricardo/casegen/output/ancestro.A.0singleSubClassOfContract_contraction_1.json",*/
        "/home/ricardo/casegen/output/pe.A.0singleSubClassOfContract_contraction_2.json",
        "/home/ricardo/casegen/output/pdo.A.0singleSubClassOfContract_contraction_3.json",
        "/home/ricardo/casegen/output/aeo.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/canco.A.1inconsistentFromDisjointSuper_consistentRevision_2.json",
        "/home/ricardo/casegen/output/phylont.A.1inconsistentFromDisjointSuper_consistentRevision_3.json",
        "/home/ricardo/casegen/output/surgical.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/pmr.A.0singleSubClassOfContract_contraction_2.json",
        "/home/ricardo/casegen/output/typon.A.0singleSubClassOfContract_contraction_3.json",
        "/home/ricardo/casegen/output/fao.A.1inconsistentFromDisjointSuper_consistentRevision_1.json",
        "/home/ricardo/casegen/output/wikipathways.A.1inconsistentFromDisjointSuper_consistentRevision_2.json",
        "/home/ricardo/casegen/output/npi.A.1inconsistentFromDisjointSuper_consistentRevision_3.json",
        "/home/ricardo/casegen/output/triage.A.0singleSubClassOfContract_contraction_1.json",
        "/home/ricardo/casegen/output/provo.A.0singleSubClassOfContract_contraction_2.json",
        "/home/ricardo/casegen/output/mamo.A.0singleSubClassOfContract_contraction_3.json"
    };

    for (String path : paths) {
      System.out.println(path);
      System.setProperty("caseinfo", path);
      System.setProperty("timeoutMillis", "300000");

      DebugState state = new DebugState();
      DebugBenchmark debugBenchmark = new DebugBenchmark();

      debugBenchmark.debug_maxNons(state);
      debugBenchmark.debug_minImps(state);
      debugBenchmark.debug_singleMaxNon(state);
      debugBenchmark.debug_singleMinImp(state);
    }

    System.out.println("END");

    /*MyBenchmarkState state = new MyBenchmarkState();
    MyBenchMark benchMark = new MyBenchMark();

    benchMark.basicMaxNons(state);
    benchMark.moduleUpdateMaxNons(state);
    benchMark.sMEMaxNons(state);

    benchMark.basicMinImps(state);
    benchMark.moduleUpdateMinImps(state);
    benchMark.sMEMinImps(state);

    benchMark.singleNodeBasicMaxNonsQueue(state);
    benchMark.singleNodeSMEMaxNonsQueue(state);

    benchMark.singlePathBasicMinImpsQueue(state);
    benchMark.singlePathSMEMinImpsQueue(state);
    benchMark.singlePathModuleUpdateMinImpsQueue(state);

    benchMark.singleNodeBasicMaxNonsStack(state);
    benchMark.singleNodeSMEMaxNonsStack(state);

    benchMark.singlePathBasicMinImpsStack(state);
    benchMark.singlePathSMEMinImpsStack(state);
    benchMark.singlePathModuleUpdateMinImpsStack(state);*/
  }
}
