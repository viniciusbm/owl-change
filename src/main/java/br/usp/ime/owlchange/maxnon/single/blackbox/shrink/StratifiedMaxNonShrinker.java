/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.maxnon.single.blackbox.shrink;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import com.google.common.collect.Sets;
import java.util.Iterator;
import java.util.Optional;
import java.util.Set;
import org.semanticweb.owlapi.model.OWLAxiom;

public class StratifiedMaxNonShrinker implements MaxNonShrinker {

  private Strata<OWLAxiom> strata;

  public StratifiedMaxNonShrinker(Strata<OWLAxiom> strata) {
    this.strata = strata;
  }

  public Strata<OWLAxiom> getStrata() {
    return strata;
  }

  public void setStrata(Strata<OWLAxiom> strata) {
    this.strata = strata;
  }

  @Override
  public Optional<Set<OWLAxiom>> shrink(Set<OWLAxiom> ontology, OntologyPropertyChecker checker,
      Set<OWLAxiom> lowerBound) {

    Set<OWLAxiom> result = Sets.newHashSet(lowerBound);

    if (checker.hasProperty(result)) {
      return Optional.empty();
    }

    result.addAll(ontology);

    Iterator<Set<OWLAxiom>> stratumIterator = strata.reverseIterator();

    while (stratumIterator.hasNext() && checker.hasProperty(result)) {
      result.removeAll(Sets.difference(stratumIterator.next(), lowerBound));
    }

    return Optional.ofNullable(!checker.hasProperty(result) ? result : null);
  }
}
