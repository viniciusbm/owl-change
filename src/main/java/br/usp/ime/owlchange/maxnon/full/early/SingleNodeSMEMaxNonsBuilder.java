/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.maxnon.full.early;

import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.maxnon.full.mod.SingleMEMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

public class SingleNodeSMEMaxNonsBuilder extends SingleMEMaxNonsBuilder {

  public SingleNodeSMEMaxNonsBuilder(MaxNonBuilder maxNonBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier,
      SyntacticLocalityModuleExtractor moduleExtractor, Set<OWLEntity> inputSignature) {
    super(maxNonBuilder, hsDequeSupplier, moduleExtractor, inputSignature);
  }

  @Override
  public RepairResult<OWLAxiom> maxNons(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    Set<OWLAxiom> complement = Sets.difference(ontology, module);
    SingleNodeDefaultMaxNonsBuilder maxNonsBuilder = new SingleNodeDefaultMaxNonsBuilder(
        maxNonBuilder, hsDequeSupplier);
    RepairResult<OWLAxiom> tmpResult = maxNonsBuilder.maxNons(this.module, checker);
    return new RepairResult<>(tmpResult.getNodes().stream().map(x -> Sets.union(x, complement))
        .collect(Collectors.toSet()), tmpResult.getPaths());
  }
}
