/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.maxnon.full.early;

import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.maxnon.full.HittingSetMaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import com.google.common.collect.ImmutableSet;
import java.util.Set;
import java.util.function.Supplier;
import org.semanticweb.owlapi.model.OWLAxiom;

public class SingleNodeDefaultMaxNonsBuilder extends HittingSetMaxNonsBuilder {

  public SingleNodeDefaultMaxNonsBuilder(MaxNonBuilder maxNonBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier) {
    super(maxNonBuilder, hsDequeSupplier);
  }

  @Override
  public RepairResult<OWLAxiom> maxNons(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    SimpleMaxNonHST hst = new FirstNodeOnlyHST(hsDequeSupplier.get(), maxNonBuilder, ontology,
        checker);
    RepairResult<OWLAxiom> result = hst.hittingSet();
    MeasurementManager.tag("end");
    return result;
  }

  public static class FirstNodeOnlyHST extends SimpleMaxNonHST {

    public FirstNodeOnlyHST(
        HSDeque<ImmutableSet<OWLAxiom>> queue,
        MaxNonBuilder maxNonBuilder, Set<OWLAxiom> ontology,
        OntologyPropertyChecker checker) {
      super(queue, maxNonBuilder, ontology, checker);
    }

    @Override
    protected boolean earlyReturn() {
      return !nodes.isEmpty();
    }

    @Override
    protected boolean shouldNotTerminate(ImmutableSet<OWLAxiom> hittingPath) {
      return nodes.isEmpty() && super.shouldNotTerminate(hittingPath);
    }
  }

}
