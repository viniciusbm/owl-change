/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.maxnon.full;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.OWLAxiom;

/* Computes the best "lexicographic" MaxNons using a stratified HST implementation.
The property must be monotonic */
public class StratifiedHSMaxNonsBuilder implements MaxNonsBuilder {

  private Strata<OWLAxiom> strata;
  protected final MaxNonBuilder maxNonBuilder;
  protected final Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier;

  public StratifiedHSMaxNonsBuilder(MaxNonBuilder maxNonBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier,
      Strata<OWLAxiom> strata) {
    this.strata = strata;
    this.maxNonBuilder = maxNonBuilder;
    this.hsDequeSupplier = Objects.requireNonNull(hsDequeSupplier);
  }

  @Override
  public RepairResult<OWLAxiom> maxNons(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    StratifiedMaxNonHST hst = new StratifiedMaxNonHST(hsDequeSupplier.get(), maxNonBuilder, strata,
        checker);
    RepairResult<OWLAxiom> result = hst.hittingSet();
    MeasurementManager.tag("end");
    return result;
  }

  public static class StratifiedMaxNonHST extends HittingSetCalculator<OWLAxiom> {

    protected final MaxNonBuilder maxNonBuilder;
    protected final Strata<OWLAxiom> strata;
    protected final OntologyPropertyChecker checker;
    protected final Set<OWLAxiom> ontology;
    protected Set<OWLAxiom> currentStratum;
    protected Integer currentBest = -1;

    public StratifiedMaxNonHST(HSDeque<ImmutableSet<OWLAxiom>> queue, MaxNonBuilder maxNonBuilder,
        Strata<OWLAxiom> strata, OntologyPropertyChecker checker) {
      super(queue);
      this.maxNonBuilder = maxNonBuilder;
      this.strata = strata;
      this.checker = checker;
      this.ontology = Sets.newHashSet();
    }

    @Override
    public HittingSetCalculator.RepairResult<OWLAxiom> hittingSet() {
      for (Set<OWLAxiom> stratum : strata) {
        this.currentStratum = stratum;
        this.ontology.addAll(stratum);

        for (Set<OWLAxiom> node : nodes) {
          for (OWLAxiom axiom : stratum) {
            deque.addNew(ImmutableSet.copyOf(Sets.union(node, Sets.newHashSet(axiom))));
          }
        }

        if (nodes.isEmpty()) {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          deque.addNew(emptyPath);
        }

        /* What if in the past iteration no axiom could be added!?
         *  It is simple, leave the node cleanup to the getNode function. When a new node is computed
         * its score should be strictly larger than all the nodes currently in getNode, and thus it
         * will make a call to nodes.clear(), before adding itself. If no node is found, then the set
         * is kept as it is and the new stratum will still be joined with the best nodes up to now.*/

        hittingSetStep();
        closedPaths.clear();
      }
      return new RepairResult<>(nodes, Collections.emptySet());
    }

    @Override
    protected Optional<Set<OWLAxiom>> reusable(ImmutableSet<OWLAxiom> hittingPath) {

      return this.nodes.parallelStream()
          .filter(e -> e.containsAll(hittingPath)).findAny();
    }

    @Override
    protected Optional<Set<OWLAxiom>> getNode(ImmutableSet<OWLAxiom> hittingPath) {

      Optional<Set<OWLAxiom>> optNode = maxNonBuilder
          .maxNon(this.ontology, checker, ImmutableSet.copyOf(hittingPath));

      if (optNode.isPresent()) {
        if (optNode.get().size() > currentBest) {
          currentBest = optNode.get().size();
          nodes.clear();
        }
        if (optNode.get().size() == currentBest) {
          nodes.add(optNode.get());
        }
      }
      return optNode;
    }

    @Override
    protected Stream<ImmutableSet<OWLAxiom>> successors(ImmutableSet<OWLAxiom> hittingPath,
        Set<OWLAxiom> node) {
      // Use only the current stratum
      return this.currentStratum.stream().filter(((Predicate<OWLAxiom>) node::contains).negate())
          .map(ImmutableSet::of)
          .map(set -> Sets.union(hittingPath, set)).map(ImmutableSet::copyOf);
    }
  }
}
