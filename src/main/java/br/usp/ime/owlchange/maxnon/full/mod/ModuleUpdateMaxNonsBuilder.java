/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.maxnon.full.mod;

import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.maxnon.full.MaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.semanticweb.owlapi.model.HasSignature;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

/* WARNING: We recently proved that his method is worthless!
 * TODO: Delete
 * */
@Deprecated
public class ModuleUpdateMaxNonsBuilder implements MaxNonsBuilder {

  protected final MaxNonBuilder maxNonBuilder;
  private final Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier;
  protected final OWLOntologyManager manager;
  protected final Set<OWLEntity> inputSignature;

  public ModuleUpdateMaxNonsBuilder(MaxNonBuilder maxNonBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier,
      OWLOntologyManager manager, Set<OWLEntity> inputSignature) {
    this.maxNonBuilder = maxNonBuilder;
    this.manager = manager;
    this.inputSignature = inputSignature;
    this.hsDequeSupplier = Objects.requireNonNull(hsDequeSupplier);
  }

  @Override
  public RepairResult<OWLAxiom> maxNons(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    ModuleUpdateMaxNonHST hst = new ModuleUpdateMaxNonHST(hsDequeSupplier.get(), maxNonBuilder,
        ontology, checker, manager, inputSignature);
    RepairResult<OWLAxiom> result = hst.hittingSet();
    MeasurementManager.tag("end");
    return result;
  }

  public static class ModuleUpdateMaxNonHST extends HittingSetCalculator<OWLAxiom> {

    protected final MaxNonBuilder maxNonBuilder;
    protected final Set<OWLAxiom> ontology;
    protected final OntologyPropertyChecker checker;
    protected final OWLOntologyManager originalManager;
    protected Set<OWLAxiom> module;
    protected ImmutableSet<OWLAxiom> initialModule;
    protected ImmutableSet<OWLAxiom> previousPath;
    protected final Set<OWLEntity> inputSignature;
    private int worthUpdates = 0;

    public ModuleUpdateMaxNonHST(HSDeque<ImmutableSet<OWLAxiom>> queue, MaxNonBuilder maxNonBuilder,
        Set<OWLAxiom> ontology, OntologyPropertyChecker checker, OWLOntologyManager manager,
        Set<OWLEntity> inputSignature) {
      super(queue);
      this.maxNonBuilder = maxNonBuilder;
      this.ontology = ontology;
      this.checker = checker;
      this.originalManager = manager;
      this.inputSignature = inputSignature;
      SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
          this.originalManager, this.ontology.stream(), ModuleType.STAR);
      this.module = moduleExtractor.extract(inputSignature);
      this.initialModule = ImmutableSet.copyOf(this.module);
      MeasurementManager.setResult("module.size", initialModule.size(), "x");
      this.previousPath = ImmutableSet.<OWLAxiom>builder().build();
    }

    @Override
    protected Optional<Set<OWLAxiom>> reusable(ImmutableSet<OWLAxiom> hittingPath) {
      return this.nodes.parallelStream().filter(e -> e.containsAll(hittingPath)).findAny();
    }

    @Override
    protected Optional<Set<OWLAxiom>> getNode(ImmutableSet<OWLAxiom> hittingPath) {

      int pastModuleSize = this.module.size();
      // Remove axioms from the previous hitting path
      SyntacticLocalityModuleExtractor shrinkExtractor = new SyntacticLocalityModuleExtractor(
          originalManager,
          Sets.difference(this.module, Sets.union(hittingPath, previousPath)).stream(),
          ModuleType.STAR);
      this.module = shrinkExtractor.extract(inputSignature);
      boolean shrinkWorth = (pastModuleSize != this.module.size());
      pastModuleSize = this.module.size();

      // Add axioms from the current hitting path (but none beyond the initial module)
      SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
          originalManager, Sets.difference(initialModule, module).stream(), ModuleType.STAR);

      Set<OWLEntity> newSignature = Sets.union(this.inputSignature,
          Sets.union(hittingPath, module).stream().flatMap(HasSignature::signature)
              .collect(Collectors.toSet()));

      this.module.addAll(moduleExtractor.extract(newSignature));

      if (shrinkWorth || this.module.size() != pastModuleSize) {
        MeasurementManager.setResult("worth_update", ++worthUpdates, "x");
      }

      Optional<Set<OWLAxiom>> optNode = this.maxNonBuilder
          .maxNon(module, checker, ImmutableSet.copyOf(hittingPath));
      this.previousPath = hittingPath;
      // add axioms in (O \ M) to the node
      if (optNode.isPresent()) {
        optNode = optNode.map(x -> Sets.union(x, Sets.difference(ontology, module)));
      }
      return optNode;
    }

    @Override
    protected Stream<ImmutableSet<OWLAxiom>> successors(ImmutableSet<OWLAxiom> hittingPath,
        Set<OWLAxiom> node) {
      return ontology.stream().filter(((Predicate<OWLAxiom>) node::contains).negate())
          .map(ImmutableSet::of)
          .map(set -> Sets.union(hittingPath, set)).map(ImmutableSet::copyOf);
    }
  }

}
