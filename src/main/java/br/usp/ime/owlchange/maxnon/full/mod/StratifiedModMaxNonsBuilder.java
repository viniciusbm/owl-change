/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.maxnon.full.mod;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.maxnon.full.MaxNonsBuilder;
import br.usp.ime.owlchange.maxnon.full.StratifiedHSMaxNonsBuilder.StratifiedMaxNonHST;
import br.usp.ime.owlchange.maxnon.single.MaxNonBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

public class StratifiedModMaxNonsBuilder implements MaxNonsBuilder {

  private Strata<OWLAxiom> strata;
  protected MaxNonBuilder maxNonBuilder;
  protected final OWLOntologyManager manager;
  protected final Set<OWLEntity> inputSignature;
  protected final Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier;

  public StratifiedModMaxNonsBuilder(MaxNonBuilder maxNonBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier, Strata<OWLAxiom> strata,
      OWLOntologyManager manager, Set<OWLEntity> inputSignature) {
    this.maxNonBuilder = maxNonBuilder;
    this.hsDequeSupplier = hsDequeSupplier;
    this.strata = strata;

    this.manager = manager;
    this.inputSignature = inputSignature;
  }

  @Override
  public RepairResult<OWLAxiom> maxNons(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    StratifiedModMaxNonHST hst = new StratifiedModMaxNonHST(hsDequeSupplier.get(), maxNonBuilder,
        strata, checker, manager, inputSignature);
    return hst.hittingSet();
  }

  public static class StratifiedModMaxNonHST extends StratifiedMaxNonHST {

    protected SyntacticLocalityModuleExtractor moduleExtractor;
    protected final Set<OWLEntity> inputSignature;
    protected OWLOntologyManager manager;

    public StratifiedModMaxNonHST(
        HSDeque<ImmutableSet<OWLAxiom>> queue,
        MaxNonBuilder maxNonBuilder, Strata<OWLAxiom> strata,
        OntologyPropertyChecker checker,
        OWLOntologyManager manager, Set<OWLEntity> inputSignature) {
      super(queue, maxNonBuilder, strata, checker);
      this.inputSignature = inputSignature;
      this.manager = manager;
    }

    @Override
    public HittingSetCalculator.RepairResult<OWLAxiom> hittingSet() {
      Set<OWLEntity> signature = Sets.newHashSet(inputSignature);
      Set<OWLAxiom> leftovers = Sets.newHashSet();
      for (Set<OWLAxiom> stratum : strata) {
        leftovers.addAll(stratum);
        SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
            this.manager, leftovers.stream(), ModuleType.STAR);
        Set<OWLAxiom> module = moduleExtractor.extract(signature);
        this.currentStratum = Sets.intersection(stratum, module);
        this.ontology.addAll(module);
        leftovers.removeAll(module);

        for (Set<OWLAxiom> node : nodes) {
          for (OWLAxiom axiom : this.currentStratum) {
            deque.addNew(ImmutableSet.copyOf(Sets.union(node, Sets.newHashSet(axiom))));
          }
        }

        if (nodes.isEmpty()) {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          deque.addNew(emptyPath);
        }

        /* What if in the past iteration no axiom could be added!?
         *  It is simple, leave the node cleanup to the getNode function. When a new node is computed
         * its score should be strictly larger than all the nodes currently in getNode, and thus it
         * will make a call to nodes.clear(), before adding itself. If no node is found, then the set
         * is kept as it is and the new stratum will still be joined with the best nodes up to now.*/
        /*if (validPaths.isEmpty()) {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          deque.addNew(emptyPath);
        }
        validPaths.clear();*/
        hittingSetStep();
        closedPaths.clear();
      }
      
      Set<Set<OWLAxiom>> restored = nodes.stream().map(n -> Sets.union(n, leftovers))
          .collect(Collectors.toSet());
      return new RepairResult<>(restored, Collections.emptySet());
    }
  }
}
