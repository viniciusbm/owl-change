/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.minimp.full.mod;

import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.minimp.full.HittingSetMinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.MinImpsBuilder;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import com.google.common.collect.ImmutableSet;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

public class SingleMEMinImpsBuilder implements MinImpsBuilder {

  protected Set<OWLAxiom> module;
  protected MinImpBuilder minImpBuilder;
  protected final Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier;

  public SingleMEMinImpsBuilder(MinImpBuilder minImpBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier,
      SyntacticLocalityModuleExtractor moduleExtractor, Set<OWLEntity> inputSignature) {
    this.module = moduleExtractor.extract(inputSignature);
    MeasurementManager.setResult("module.size", this.module.size(), "x");
    this.minImpBuilder = minImpBuilder;
    this.hsDequeSupplier = Objects.requireNonNull(hsDequeSupplier);
  }

  @Override
  public RepairResult<OWLAxiom> minImps(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    HittingSetMinImpsBuilder minImpsBuilder = new HittingSetMinImpsBuilder(minImpBuilder,
        hsDequeSupplier);
    return minImpsBuilder.minImps(this.module, checker);
  }
}
