/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.minimp.full.mod;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.minimp.full.HittingSetMinImpsBuilder.SimpleMinImpHST;
import br.usp.ime.owlchange.minimp.full.MinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.StratifiedHSMinImpsBuilder.LexicographicScore;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import org.semanticweb.owlapi.model.HasSignature;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

public class StratifiedMUMinImpsBuilder implements MinImpsBuilder {

  private Strata<OWLAxiom> strata;
  protected final MinImpBuilder minImpBuilder;
  protected final OWLOntologyManager manager;
  protected final Set<OWLEntity> inputSignature;
  protected final Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier;

  public StratifiedMUMinImpsBuilder(MinImpBuilder minImpBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier, Strata<OWLAxiom> strata,
      OWLOntologyManager manager, Set<OWLEntity> inputSignature) {
    this.minImpBuilder = minImpBuilder;
    this.manager = manager;
    this.inputSignature = inputSignature;
    this.hsDequeSupplier = Objects.requireNonNull(hsDequeSupplier);
    this.strata = strata;
  }

  @Override
  public RepairResult<OWLAxiom> minImps(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    StratifiedMUMinImpsHST hst = new StratifiedMUMinImpsHST(hsDequeSupplier.get(), minImpBuilder,
        strata, ontology, checker, manager, inputSignature);

    RepairResult<OWLAxiom> result = hst.hittingSet();
    MeasurementManager.tag("end");
    return result;
  }

  public static class StratifiedMUMinImpsHST extends SimpleMinImpHST {

    protected final MinImpBuilder minImpBuilder;
    protected final Set<OWLAxiom> ontology;
    protected final OntologyPropertyChecker checker;
    protected final OWLOntologyManager originalManager;

    protected final Strata<OWLAxiom> strata;
    LexicographicScore currentBest = null;
    Set<ImmutableSet<OWLAxiom>> validPaths;
    int scoreLength = 0;

    protected Set<OWLAxiom> module;
    protected ImmutableSet<OWLAxiom> initialModule;
    protected ImmutableSet<OWLAxiom> previousPath;
    protected final Set<OWLEntity> inputSignature;

    protected int worthUpdates = 0;
    protected int iter = 0;

    public StratifiedMUMinImpsHST(HSDeque<ImmutableSet<OWLAxiom>> queue,
        MinImpBuilder minImpBuilder, Strata<OWLAxiom> strata,
        Set<OWLAxiom> ontology, OntologyPropertyChecker checker, OWLOntologyManager manager,
        Set<OWLEntity> inputSignature) {
      super(queue, minImpBuilder, ontology, checker);
      this.minImpBuilder = minImpBuilder;
      this.ontology = Sets.newHashSet();
      this.checker = checker;
      this.strata = strata;
      this.originalManager = manager;
      this.inputSignature = inputSignature;
      SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
          this.originalManager, ontology.stream(), ModuleType.STAR);
      this.module = moduleExtractor.extract(inputSignature);
      this.initialModule = ImmutableSet.copyOf(this.module);
      MeasurementManager.setResult("module.size", initialModule.size(), "x");
      this.previousPath = ImmutableSet.<OWLAxiom>builder().build();
      this.validPaths = Sets.newHashSet();
    }

    @Override
    protected Optional<Set<OWLAxiom>> getNode(ImmutableSet<OWLAxiom> hittingPath) {

      // Remove axioms from the current hitting path
      SyntacticLocalityModuleExtractor shrinkExtractor = new SyntacticLocalityModuleExtractor(
          originalManager, Sets.difference(this.module, hittingPath).stream(), ModuleType.STAR);
      this.module = shrinkExtractor.extract(inputSignature);

      // Add axioms from the previous hitting path (but none beyond the initial module)
      SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
          originalManager, Sets.difference(Sets.difference(initialModule, hittingPath)
          , module).stream(), ModuleType.STAR);
      Set<OWLEntity> newSignature = Sets.union(this.inputSignature,
          module.stream().flatMap(HasSignature::signature).collect(Collectors.toSet()));
      this.module.addAll(moduleExtractor.extract(newSignature));

      if (this.module.size() < (initialModule.size() - hittingPath.size())) {
        MeasurementManager.setResult("update_worth", ++worthUpdates, "x");
      }
      MeasurementManager.setResult("iter", ++iter, "x");

      Optional<Set<OWLAxiom>> optNode = this.minImpBuilder.minImp(module, checker);
      this.previousPath = hittingPath;
      return optNode;
    }

    public HittingSetCalculator.RepairResult<OWLAxiom> hittingSet() {
      for (Set<OWLAxiom> stratum : strata) {
        scoreLength++;
        this.ontology.addAll(Sets.intersection(stratum, this.initialModule));
        if (!validPaths.isEmpty()) {
          validPaths.forEach(deque::addNew);
        } else if (deque.isEmpty()) {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          deque.addNew(emptyPath);
        }
        hittingSetStep();
        currentBest = null;
        closedPaths.clear();
      }
      return new RepairResult<>(nodes, validPaths);
    }
  }

}
