/*
 *    Copyright 2018-2019 OWL2DL-Change Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */
package br.usp.ime.owlchange.minimp.full;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Optional;
import java.util.Set;
import java.util.function.Supplier;
import java.util.stream.Stream;
import org.checkerframework.checker.nullness.qual.NonNull;
import org.semanticweb.owlapi.model.OWLAxiom;

/* Computes the best "lexicographic" MinImps using a stratified HST implementation.
The property must be monotonic */
public class StratifiedHSMinImpsBuilder implements MinImpsBuilder {

  private Strata<OWLAxiom> strata;
  protected final MinImpBuilder minImpBuilder;
  protected final Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier;

  public StratifiedHSMinImpsBuilder(MinImpBuilder minImpBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier,
      Strata<OWLAxiom> strata) {
    this.strata = strata;
    this.minImpBuilder = minImpBuilder;
    this.hsDequeSupplier = hsDequeSupplier;
  }

  @Override
  public RepairResult<OWLAxiom> minImps(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {

    StratifiedMinImpHST hst = new StratifiedMinImpHST(hsDequeSupplier.get(), minImpBuilder, strata,
        checker);

    RepairResult<OWLAxiom> result = hst.hittingSet();
    MeasurementManager.tag("end");
    return result;
  }

  public static class StratifiedMinImpHST extends HittingSetCalculator<OWLAxiom> {

    protected final MinImpBuilder minImpBuilder;
    protected final Strata<OWLAxiom> strata;
    protected final OntologyPropertyChecker checker;
    protected final Set<OWLAxiom> ontology;
    protected LexicographicScore currentBest = null;
    protected Set<ImmutableSet<OWLAxiom>> validPaths;
    protected int scoreLength = 0;

    public StratifiedMinImpHST(HSDeque<ImmutableSet<OWLAxiom>> queue,
        MinImpBuilder minImpBuilder, Strata<OWLAxiom> strata,
        OntologyPropertyChecker checker) {
      super(queue);
      this.minImpBuilder = minImpBuilder;
      this.strata = strata;
      this.checker = checker;
      this.ontology = Sets.newHashSet();
      this.validPaths = Sets.newHashSet();
    }

    @Override
    public HittingSetCalculator.RepairResult<OWLAxiom> hittingSet() {
      for (Set<OWLAxiom> stratum : strata) {
        scoreLength++;
        this.ontology.addAll(stratum);
        if (!validPaths.isEmpty()) {
          validPaths.forEach(deque::addNew);
        } else if (deque.isEmpty()) {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          deque.addNew(emptyPath);
        }
        hittingSetStep();
        currentBest = null;
        closedPaths.clear();
      }
      return new RepairResult<>(nodes, validPaths);
    }

    @Override
    protected Optional<Set<OWLAxiom>> reusable(ImmutableSet<OWLAxiom> hittingPath) {
      return this.nodes.parallelStream().filter(e -> Sets.intersection(e, hittingPath).isEmpty())
          .findAny();
    }

    // TODO: be careful of concurrency and side effects of changes in the ontology
    @Override
    protected Optional<Set<OWLAxiom>> getNode(ImmutableSet<OWLAxiom> hittingPath) {
      return minImpBuilder.minImp(Sets.difference(ontology, hittingPath), checker);
    }

    @Override
    protected Stream<ImmutableSet<OWLAxiom>> successors(ImmutableSet<OWLAxiom> hittingPath,
        Set<OWLAxiom> node) {

      return node.stream().map(Sets::newHashSet).map(set -> Sets.union(hittingPath, set))
          .map(ImmutableSet::copyOf);

    }

    @Override
    protected void close(ImmutableSet<OWLAxiom> hittingPath) {
      super.close(hittingPath);
      final LexicographicScore newScore = new LexicographicScore(this.scoreLength);
      hittingPath.stream().map(axiom -> strata.getRank(axiom).orElseThrow(RuntimeException::new))
          .forEach(
              newScore::inc);

      if (currentBest == null || currentBest.compareTo(newScore) > 0) {
        validPaths.clear();
        currentBest = newScore;
      }

      if (currentBest.compareTo(newScore) == 0) {
        validPaths.add(hittingPath);
      }
    }
  }

  public static class LexicographicScore implements Comparable<LexicographicScore> {

    private long[] score;

    public LexicographicScore(int scoreLength) {
      this.score = new long[scoreLength];
    }

    public LexicographicScore(LexicographicScore oldScore) {
      this.score = oldScore.getScore().clone();
    }

    public void inc(int position) {
      score[position] += 1;
    }

    @Override
    public int compareTo(@NonNull LexicographicScore other) {

      for (int i = 0; i < this.score.length; i++) {
        if (this.score[i] != other.score[i]) {
          return Long.compare(this.score[i], other.score[i]);
        }

      }
      return 0;
    }

    public long[] getScore() {
      return score;
    }
  }
}
