/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.minimp.full.early;

import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.minimp.full.mod.ModuleUpdateMinImpsBuilder;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import com.google.common.collect.ImmutableSet;
import java.util.Set;
import java.util.function.Supplier;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public class SinglePathMUMinImpsBuilder extends ModuleUpdateMinImpsBuilder {

  public SinglePathMUMinImpsBuilder(MinImpBuilder minImpBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier,
      OWLOntologyManager manager,
      Set<OWLEntity> inputSignature) {
    super(minImpBuilder, hsDequeSupplier, manager, inputSignature);
  }

  @Override
  public RepairResult<OWLAxiom> minImps(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {
    SinglePathMUMinImpsHST hst = new SinglePathMUMinImpsHST(hsDequeSupplier.get(), minImpBuilder,
        ontology, checker, manager, inputSignature);
    RepairResult<OWLAxiom> result = hst.hittingSet();
    MeasurementManager.tag("end");
    return result;
  }

  public static class SinglePathMUMinImpsHST extends
      ModuleUpdateMinImpsBuilder.ModuleUpdateMinImpsHST {

    public SinglePathMUMinImpsHST(
        HSDeque<ImmutableSet<OWLAxiom>> queue,
        MinImpBuilder minImpBuilder, Set<OWLAxiom> ontology,
        OntologyPropertyChecker checker,
        OWLOntologyManager manager,
        Set<OWLEntity> inputSignature) {
      super(queue, minImpBuilder, ontology, checker, manager, inputSignature);
    }

    @Override
    protected boolean earlyReturn() {
      return !closedPaths.isEmpty();
    }
  }
}
