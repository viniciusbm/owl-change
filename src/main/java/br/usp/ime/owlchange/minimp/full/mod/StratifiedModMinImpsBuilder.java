/*
 *    Copyright 2018-2019 OWL2DL-Change-Modularisation Developers
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

package br.usp.ime.owlchange.minimp.full.mod;

import br.usp.ime.owl2dlstratification.strata.Strata;
import br.usp.ime.owlchange.MeasurementManager;
import br.usp.ime.owlchange.OntologyPropertyChecker;
import br.usp.ime.owlchange.hst.HSDeque;
import br.usp.ime.owlchange.hst.HittingSetCalculator;
import br.usp.ime.owlchange.hst.HittingSetCalculator.RepairResult;
import br.usp.ime.owlchange.minimp.full.MinImpsBuilder;
import br.usp.ime.owlchange.minimp.full.StratifiedHSMinImpsBuilder.StratifiedMinImpHST;
import br.usp.ime.owlchange.minimp.single.MinImpBuilder;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Sets;
import java.util.Collections;
import java.util.Objects;
import java.util.Set;
import java.util.function.Supplier;
import org.semanticweb.owlapi.model.HasSignature;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLEntity;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import uk.ac.manchester.cs.owlapi.modularity.ModuleType;
import uk.ac.manchester.cs.owlapi.modularity.SyntacticLocalityModuleExtractor;

public class StratifiedModMinImpsBuilder implements MinImpsBuilder {

  private Strata<OWLAxiom> strata;
  protected final MinImpBuilder minImpBuilder;
  protected final OWLOntologyManager manager;
  protected final Set<OWLEntity> inputSignature;
  protected final Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier;

  public StratifiedModMinImpsBuilder(MinImpBuilder minImpBuilder,
      Supplier<? extends HSDeque<ImmutableSet<OWLAxiom>>> hsDequeSupplier, Strata<OWLAxiom> strata,
      OWLOntologyManager manager, Set<OWLEntity> inputSignature) {
    this.strata = strata;
    this.minImpBuilder = minImpBuilder;
    this.hsDequeSupplier = Objects.requireNonNull(hsDequeSupplier);

    this.manager = manager;
    this.inputSignature = inputSignature;
  }

  @Override
  public RepairResult<OWLAxiom> minImps(Set<OWLAxiom> ontology, OntologyPropertyChecker checker) {

    StratifiedModMinImpsHST hst = new StratifiedModMinImpsHST(hsDequeSupplier.get(), minImpBuilder,
        strata, checker, this.manager, this.inputSignature);

    RepairResult<OWLAxiom> result = hst.hittingSet();
    MeasurementManager.tag("end");
    return result;
  }

  public static class StratifiedModMinImpsHST extends StratifiedMinImpHST {

    protected SyntacticLocalityModuleExtractor moduleExtractor;
    protected final Set<OWLEntity> inputSignature;
    protected OWLOntologyManager manager;

    public StratifiedModMinImpsHST(HSDeque<ImmutableSet<OWLAxiom>> queue,
        MinImpBuilder minImpBuilder, Strata<OWLAxiom> strata,
        OntologyPropertyChecker checker,
        OWLOntologyManager manager, Set<OWLEntity> inputSignature) {
      super(queue, minImpBuilder, strata, checker);
      this.inputSignature = inputSignature;
      this.manager = manager;
      this.validPaths = Sets.newHashSet();
    }

    @Override
    public HittingSetCalculator.RepairResult<OWLAxiom> hittingSet() {
      Set<OWLEntity> signature = Sets.newHashSet(inputSignature);
      Set<OWLAxiom> leftovers = Sets.newHashSet();
      for (Set<OWLAxiom> stratum : strata) {
        this.scoreLength++;
        leftovers.addAll(stratum);
        SyntacticLocalityModuleExtractor moduleExtractor = new SyntacticLocalityModuleExtractor(
            this.manager, leftovers.stream(), ModuleType.STAR);

        Set<OWLAxiom> module = moduleExtractor.extract(signature);
        this.ontology.addAll(module);
        ontology.stream().flatMap(HasSignature::unsortedSignature).forEach(signature::add);

        if (!validPaths.isEmpty()) {
          validPaths.forEach(deque::addNew);
        } else if (deque.isEmpty()) {
          ImmutableSet<OWLAxiom> emptyPath = ImmutableSet.copyOf((Collections.emptySet()));
          deque.addNew(emptyPath);
        }
        hittingSetStep();
        currentBest = null;
        closedPaths.clear();
      }
      return new RepairResult<>(nodes, validPaths);
    }
  }
}
